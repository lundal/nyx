import * as React from "react";
import { ReactElement, useState } from "react";
import * as ReactDOM from "react-dom";
import {
  Button,
  CheckBox,
  Column,
  Dialog,
  FilePicker,
  Header,
  Heading,
  IconButton,
  Input,
  Message,
  Progress,
  RadioGroup,
  Row,
  Section,
  Spinner,
  Table,
  TextBox,
} from "../src";
import {
  faArchive,
  faBan,
  faBars,
  faCog,
  faPencilAlt,
  faTrashAlt,
} from "@fortawesome/free-solid-svg-icons";
// @ts-ignore
import logo from "url:./logo.svg";

function App(): ReactElement {
  const options = [
    { value: 1, label: "Radio 1" },
    { value: 2, label: "Radio 2" },
    { value: 3, label: "Radio 3" },
  ];

  const [textbox1, setTextbox1] = useState("");
  const [textbox2, setTextbox2] = useState("12345678");
  const [checkbox1, setCheckbox1] = useState(false);
  const [checkbox2, setCheckbox2] = useState(true);
  const [radio, setRadio] = useState(1);
  const [dialog, setDialog] = useState(false);

  const style = { maxWidth: "640px", margin: "var(--size-4) auto" };

  return (
    <>
      <Section style={style}>
        <Column>
          <Heading level={1}>Heading 1</Heading>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua.
          </p>
          <Heading level={2}>Heading 2</Heading>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua.
          </p>
          <Heading level={3}>Heading 3</Heading>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua.
          </p>
          <p>
            Lorem ipsum{" "}
            <a href="#" className="nyx-link">
              with a link
            </a>
            , sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
            nisi ut aliquip ex ea commodo consequat.
          </p>
        </Column>
      </Section>
      <Section style={style}>
        <Column>
          <Heading level={2}>TextBox</Heading>
          <Input
            id="textbox"
            label="Empty"
            help="Validation rules or some helpful advice"
            error={textbox1.length > 3 && "Validation error"}
            input={<TextBox value={textbox1} onChange={setTextbox1} />}
          />
          <Input
            id="textbox2"
            label="Validation"
            help="Enter at least 12 characters"
            error={textbox2.length < 12 && "Must be at least 12 characters"}
            input={<TextBox value={textbox2} onChange={setTextbox2} />}
          />
        </Column>
      </Section>
      <Section style={style}>
        <Column>
          <Heading level={2}>CheckBox</Heading>
          <CheckBox
            id="checkbox1"
            label="Unchecked"
            value={checkbox1}
            onChange={setCheckbox1}
          />
          <CheckBox
            id="checkbox2"
            label="Checked"
            value={checkbox2}
            onChange={setCheckbox2}
          />
        </Column>
      </Section>
      <Section style={style}>
        <Column>
          <Heading level={2}>RadioGroup</Heading>
          <RadioGroup
            id="radio"
            options={options}
            value={radio}
            onChange={setRadio}
          />
        </Column>
      </Section>
      <Section style={style}>
        <Column>
          <Heading level={2}>Progress</Heading>
          <Input label="Progress" input={<Progress current={2} total={10} />} />
          <Input label="Progress" input={<Progress current={4} total={10} />} />
          <Input
            label="Progress"
            help="Copying files..."
            input={<Progress current={8} total={10} />}
          />
        </Column>
      </Section>
      <Section style={style}>
        <Column>
          <Heading level={2}>Button</Heading>
          <Row>
            <Button label="Default" />
            <Button color="white" label="White" />
            <Button color="red" label="Red" />
            <Button color="yellow" label="Yellow" />
            <Button color="green" label="Green" />
            <Button color="blue" label="Blue" />
          </Row>
          <Row>
            <FilePicker label="Select file" accept="*/*" onChange={() => {}} />
            <Button label="Show Dialog" onClick={() => setDialog(true)} />
          </Row>
        </Column>
        {dialog && (
          <Dialog onDismiss={() => setDialog(false)}>
            <Column>
              <Heading level={2}>Dialog</Heading>
              <p>
                Focus is locked inside the dialog. The first element is focused
                when shown. The 'Tab' and 'Shift-Tab' keys will loop around the
                elements inside the dialog.
              </p>
              <p>
                You can close the dialog by clicking 'Close', clicking the
                backdrop og hitting the 'Esc' key.
              </p>
              <div />
              <Button label="Close" onClick={() => setDialog(false)} />
            </Column>
          </Dialog>
        )}
      </Section>
      <Section style={style}>
        <Column>
          <Heading level={2}>IconButton</Heading>
          <div>
            <IconButton color="transparent" size="small" icon={faBan} />
            <IconButton color="transparent" size="small" icon={faArchive} />
            <IconButton color="transparent" size="small" icon={faCog} />
          </div>
          <div>
            <IconButton color="transparent" icon={faBan} />
            <IconButton color="transparent" icon={faArchive} />
            <IconButton color="transparent" icon={faCog} />
          </div>
          <div>
            <IconButton color="transparent" size="large" icon={faBan} />
            <IconButton color="transparent" size="large" icon={faArchive} />
            <IconButton color="transparent" size="large" icon={faCog} />
          </div>
          <Row>
            <IconButton icon={faCog} />
            <IconButton color="white" icon={faCog} />
            <IconButton color="red" icon={faCog} />
            <IconButton color="yellow" icon={faCog} />
            <IconButton color="green" icon={faCog} />
            <IconButton color="blue" icon={faCog} />
          </Row>
        </Column>
      </Section>
      <Section style={style}>
        <Column>
          <Heading level={2}>Message</Heading>
          <Message type="error" text="Error message" />
          <Message type="warn" text="Warning message" />
          <Message type="info" text="Info message" />
          <Message type="success" text="Success message" />
        </Column>
      </Section>
      <Section style={style}>
        <Column>
          <Heading level={2}>Spinner</Heading>
          <Row align="center">
            <Spinner />
            <Spinner size="large" />
          </Row>
        </Column>
      </Section>
      <Section style={style}>
        <Column align="start">
          <Heading level={2}>Table</Heading>
          <Table
            head={
              <tr>
                <th>#</th>
                <th>Number</th>
              </tr>
            }
            body={[
              <tr>
                <td>11</td>
                <td>Eleven</td>
              </tr>,
              <tr>
                <td>42</td>
                <td>Forty two</td>
              </tr>,
              <tr>
                <td>91</td>
                <td>Ninety one</td>
              </tr>,
            ]}
          />
        </Column>
      </Section>
      <Section style={style}>
        <Column>
          <Heading level={2}>Header</Heading>
          <Header
            app={{ name: "Nyx", iconUrl: logo, linkUrl: "/" }}
            actions={
              <>
                <IconButton color="transparent" icon={faCog} />
                <IconButton color="transparent" icon={faBars} />
              </>
            }
          />
          <Header
            app={{ name: "Nyx", iconUrl: logo, linkUrl: "/" }}
            apps={[
              { name: "Lorem", iconUrl: logo, linkUrl: "https://example.com/" },
              { name: "Ipsum", iconUrl: logo, linkUrl: "https://example.com/" },
            ]}
            actions={
              <>
                <span>Page </span>
                <IconButton
                  color="transparent"
                  icon={faPencilAlt}
                  size="small"
                />
                <IconButton
                  color="transparent"
                  icon={faTrashAlt}
                  size="small"
                />
              </>
            }
          />
        </Column>
      </Section>
    </>
  );
}

ReactDOM.render(<App />, document.getElementById("app"));
