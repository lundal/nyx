import * as React from "react";
import { CSSProperties, ReactElement } from "react";

type ProgressProps = {
  className?: string;
  style?: CSSProperties;
  color?: "white" | "red" | "yellow" | "green" | "blue";
  id?: string;
  current: number;
  total: number;
};

export function Progress(props: ProgressProps): ReactElement {
  return (
    <progress
      className={
        "nyx-progress" +
        (props.color ? `nyx-progress--${props.color}` : "") +
        (props.className ? ` ${props.className}` : "")
      }
      style={props.style}
      id={props.id}
      value={props.current}
      max={props.total}
    />
  );
}
