import * as React from "react";
import { ReactElement } from "react";
import { Spinner } from "./Spinner";

export function LoadingLayout(): ReactElement {
  return (
    <div className="nyx-loading-layout">
      <Spinner size="large" />
    </div>
  );
}
