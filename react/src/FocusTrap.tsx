import * as React from "react";
import {
  AriaRole,
  CSSProperties,
  FocusEvent,
  MouseEvent,
  ReactElement,
  ReactNode,
  useEffect,
  useRef,
} from "react";

type FocusTrapProps = {
  role?: AriaRole;
  className?: string;
  style?: CSSProperties;
  dismissOnBlur?: boolean;
  dismissOnClick?: boolean;
  onDismiss: () => void;
  children: ReactNode;
};

export function FocusTrap(props: FocusTrapProps): ReactElement {
  const ref = useRef<HTMLDivElement | null>(null);

  function handleBlur(event: FocusEvent): void {
    if (!props.dismissOnBlur) {
      return;
    }
    const focused = event.relatedTarget;
    const focusable = getFocusableChildren(ref.current!);
    const focusInside = focusable.includes(focused as any);

    if (!focusInside) {
      props.onDismiss();
    }
  }

  function handleClick(event: MouseEvent): void {
    if (!props.dismissOnClick) {
      return;
    }
    if (event.target === ref.current) {
      props.onDismiss();
    }
  }

  function handleEscape(event: KeyboardEvent): void {
    if (event.key === "Escape") {
      props.onDismiss();
    }
  }

  function handleTab(event: KeyboardEvent): void {
    if (event.key !== "Tab") {
      return;
    }
    const focusable = getFocusableChildren(ref.current!);
    if (focusable.length < 1) {
      return;
    }
    const focused = getFocused();
    const firstFocusable = focusable[0];
    const lastFocusable = focusable[focusable.length - 1];
    const focusInside = focusable.includes(focused as any);

    // Lock focus inside
    if (event.shiftKey && (focused === firstFocusable || !focusInside)) {
      lastFocusable.focus();
      event.preventDefault();
    }
    if (!event.shiftKey && (focused === lastFocusable || !focusInside)) {
      firstFocusable.focus();
      event.preventDefault();
    }
  }

  function grabAndRestoreFocus() {
    const focused = getFocused();
    getFocusableChildren(ref.current!)[0]?.focus();
    return () => {
      getFocused() || focused?.focus();
    };
  }

  useEffect(grabAndRestoreFocus, []);

  useKeyDown(handleEscape);
  useKeyDown(handleTab);

  return (
    <div
      role={props.role}
      className={props.className}
      style={props.style}
      ref={ref}
      onBlur={handleBlur}
      onClick={handleClick}
    >
      {props.children}
    </div>
  );
}

function useKeyDown(listener: (e: KeyboardEvent) => void): void {
  useEffect(() => {
    document.addEventListener("keydown", listener);
    return () => {
      document.removeEventListener("keydown", listener);
    };
  }, [listener]);
}

// Based on https://zellwk.com/blog/keyboard-focusable-elements/
function getFocusableChildren(parent: HTMLElement): HTMLElement[] {
  const elements: HTMLElement[] = Array.from(
    parent.querySelectorAll(
      'a, button, input, textarea, select, details,[tabindex]:not([tabindex="-1"])'
    )
  );
  return elements.filter((el) => !el.hasAttribute("disabled"));
}

function getFocused(): HTMLElement | null {
  const activeElement = document.activeElement as HTMLElement | null;
  return activeElement === document.body ? null : activeElement;
}
