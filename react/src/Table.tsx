import * as React from "react";
import { ReactElement } from "react";

type TableProps = {
  head?: ReactElement;
  body: ReactElement | ReactElement[];
  foot?: ReactElement;
};

export function Table(props: TableProps): ReactElement {
  return (
    <table className="nyx-table">
      <thead>{props.head}</thead>
      <tbody>{props.body}</tbody>
      <tfoot>{props.foot}</tfoot>
    </table>
  );
}
