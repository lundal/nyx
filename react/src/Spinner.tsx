import * as React from "react";
import { CSSProperties, ReactElement } from "react";

type SpinnerProps = {
  className?: string;
  style?: CSSProperties;
  size?: "large";
};

export function Spinner(props: SpinnerProps): ReactElement {
  return (
    <div
      className={
        "nyx-spinner" +
        (props.size ? ` nyx-spinner--${props.size}` : "") +
        (props.className ? ` ${props.className}` : "")
      }
      style={props.style}
    />
  );
}
