import * as React from "react";
import { CSSProperties, ReactElement, ReactNode } from "react";

type FormProps = {
  className?: string;
  style?: CSSProperties;
  onSubmit: () => void;
  children: ReactNode;
};

export function Form(props: FormProps): ReactElement {
  return (
    <form
      className={props.className}
      style={props.style}
      onSubmit={(event) => {
        event.preventDefault();
        props.onSubmit();
      }}
    >
      {props.children}
    </form>
  );
}
