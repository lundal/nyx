import * as React from "react";
import { CSSProperties, ReactElement } from "react";

type MessageProps = {
  className?: string;
  style?: CSSProperties;
  type: "error" | "warn" | "info" | "success";
  text: string;
};

export function Message(props: MessageProps): ReactElement {
  return (
    <div
      className={
        `nyx-message nyx-message--${props.type}` +
        (props.className ? ` ${props.className}` : "")
      }
      style={props.style}
    >
      {props.text}
    </div>
  );
}
