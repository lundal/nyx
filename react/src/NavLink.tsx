import * as React from "react";
import { ReactElement } from "react";
import { NavLink as ReactRouterNavLink } from "react-router-dom";

type NavLinkProps = {
  name: string;
  url: string;
};

export function NavLink(props: NavLinkProps): ReactElement {
  return (
    <ReactRouterNavLink
      exact
      to={props.url}
      className="nyx-nav-link"
      activeClassName="nyx-nav-link--active"
    >
      {props.name}
    </ReactRouterNavLink>
  );
}
