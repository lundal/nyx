import * as React from "react";
import { CSSProperties, ReactElement } from "react";

type ButtonProps = {
  className?: string;
  style?: CSSProperties;
  color?: "transparent" | "white" | "red" | "yellow" | "green" | "blue";
  id?: string;
  label: string;
  onClick?: () => void;
};

export function Button(props: ButtonProps): ReactElement {
  return (
    <button
      className={
        "nyx-button" +
        (props.color ? ` nyx-button--${props.color}` : "") +
        (props.className ? ` ${props.className}` : "")
      }
      style={props.style}
      id={props.id}
      onClick={(event) => {
        if (props.onClick) {
          event.stopPropagation();
          event.preventDefault();
          props.onClick();
        }
      }}
      type={props.onClick ? "button" : "submit"}
    >
      {props.label}
    </button>
  );
}
