import * as React from "react";
import { ReactElement } from "react";

type NavLayoutProps = {
  header: ReactElement;
  nav: ReactElement;
  main: ReactElement;
};

export function NavLayout(props: NavLayoutProps): ReactElement {
  return (
    <div className="nyx-nav-layout">
      {props.header}
      {props.nav}
      {props.main}
    </div>
  );
}
