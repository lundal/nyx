import * as React from "react";
import { cloneElement, CSSProperties, ReactElement } from "react";

type InputProps = {
  className?: string;
  style?: CSSProperties;
  id?: string;
  label?: string;
  input: ReactElement;
  help?: string;
  error?: string | false;
};

export function Input(props: InputProps): ReactElement {
  return (
    <div
      className={"nyx-input" + (props.className ? ` ${props.className}` : "")}
      style={props.style}
    >
      {props.label && (
        <label htmlFor={props.id} className="nyx-label">
          {props.label}
        </label>
      )}
      {cloneElement(props.input, {
        id: props.id,
        error: !!props.error,
      })}
      {!props.error && props.help && (
        <div className="nyx-help">{props.help}</div>
      )}
      {props.error && <div className="nyx-error">{props.error}</div>}
    </div>
  );
}
