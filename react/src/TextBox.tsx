import * as React from "react";
import { CSSProperties, ReactElement } from "react";

type TextBoxProps = {
  className?: string;
  style?: CSSProperties;
  id?: string;
  type?: "text" | "password";
  autoComplete?: string;
  error?: boolean;
  value: string;
  onChange: (value: string) => void;
};

export function TextBox(props: TextBoxProps): ReactElement {
  return (
    <input
      className={
        "nyx-textbox" +
        (props.error ? " nyx-textbox--error" : "") +
        (props.className ? ` ${props.className}` : "")
      }
      style={props.style}
      id={props.id}
      type={props.type ?? "text"}
      autoComplete={props.autoComplete ?? "off"}
      value={props.value}
      onChange={(e) => props.onChange(e.target.value)}
    />
  );
}
