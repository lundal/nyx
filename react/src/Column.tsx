import * as React from "react";
import { CSSProperties, ReactElement, ReactHTML, ReactNode } from "react";

type ColumnProps = {
  element?: keyof ReactHTML;
  className?: string;
  style?: CSSProperties;
  align?: "start" | "center" | "end";
  spacing?: "small" | "large";
  wrap?: boolean;
  children: ReactNode;
};

export function Column(props: ColumnProps): ReactElement {
  const Element = props.element ?? "div";
  return (
    <Element
      className={
        "nyx-column" +
        (props.align ? ` nyx-column--align-${props.align}` : "") +
        (props.spacing ? ` nyx-column--spacing-${props.spacing}` : "") +
        (props.wrap ? ` nyx-column--wrap` : "") +
        (props.className ? ` ${props.className}` : "")
      }
      style={props.style}
    >
      {props.children}
    </Element>
  );
}
