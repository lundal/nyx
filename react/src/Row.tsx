import * as React from "react";
import { CSSProperties, ReactElement, ReactHTML, ReactNode } from "react";

type RowProps = {
  element?: keyof ReactHTML;
  className?: string;
  style?: CSSProperties;
  align?: "start" | "center" | "end";
  spacing?: "small" | "large";
  wrap?: boolean;
  children: ReactNode;
};

export function Row(props: RowProps): ReactElement {
  const Element = props.element ?? "div";
  return (
    <Element
      className={
        "nyx-row" +
        (props.align ? ` nyx-row--align-${props.align}` : "") +
        (props.spacing ? ` nyx-row--spacing-${props.spacing}` : "") +
        (props.wrap ? ` nyx-row--wrap` : "") +
        (props.className ? ` ${props.className}` : "")
      }
      style={props.style}
    >
      {props.children}
    </Element>
  );
}
