import * as React from "react";
import { ReactElement, ReactNode } from "react";
import { FocusTrap } from "./FocusTrap";

type DialogProps = {
  onDismiss: () => void;
  children: ReactNode;
};

export function Dialog(props: DialogProps): ReactElement {
  return (
    <FocusTrap
      role="dialog"
      className="nyx-dialog"
      dismissOnClick={true}
      onDismiss={props.onDismiss}
    >
      {props.children}
    </FocusTrap>
  );
}
