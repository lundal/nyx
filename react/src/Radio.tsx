import * as React from "react";
import { ReactElement } from "react";

type RadioProps = {
  id?: string;
  label?: string;
  selected: boolean;
  onSelected: () => void;
};

export function Radio(props: RadioProps): ReactElement {
  return (
    <div>
      <input
        id={props.id}
        type="radio"
        className="nyx-radio"
        checked={props.selected}
        onChange={props.onSelected}
      />
      {props.label && <label htmlFor={props.id}>{props.label}</label>}
    </div>
  );
}
