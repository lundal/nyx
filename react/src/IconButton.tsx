import * as React from "react";
import { CSSProperties, ReactElement } from "react";
import { IconLookup } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

type IconButtonProps = {
  className?: string;
  style?: CSSProperties;
  color?: "transparent" | "white" | "red" | "yellow" | "green" | "blue";
  size?: "small" | "large";
  title?: string;
  icon: IconLookup;
  onClick?: () => void;
};

export function IconButton(props: IconButtonProps): ReactElement {
  return (
    <button
      className={
        "nyx-button nyx-button--square" +
        (props.color ? ` nyx-button--${props.color}` : "") +
        (props.size ? ` nyx-button--${props.size}` : "") +
        (props.className ? ` ${props.className}` : "")
      }
      style={props.style}
      title={props.title}
      onClick={(event) => {
        if (props.onClick) {
          event.stopPropagation();
          event.preventDefault();
          props.onClick();
        }
      }}
      type={props.onClick ? "button" : "submit"}
    >
      <FontAwesomeIcon icon={props.icon} />
    </button>
  );
}
