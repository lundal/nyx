import * as React from "react";
import { CSSProperties, ReactElement, ReactHTML, ReactNode } from "react";

type SectionProps = {
  element?: keyof ReactHTML;
  className?: string;
  style?: CSSProperties;
  children: ReactNode;
};

export function Section(props: SectionProps): ReactElement {
  const Element = props.element ?? "div";
  return (
    <Element
      className={"nyx-section" + (props.className ? ` ${props.className}` : "")}
      style={props.style}
    >
      {props.children}
    </Element>
  );
}
