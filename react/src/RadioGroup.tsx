import * as React from "react";
import { Radio } from "./Radio";
import { CSSProperties, ReactElement } from "react";

type RadioGroupProps<T> = {
  className?: string;
  style?: CSSProperties;
  id?: string;
  options: { value: T; label: string }[];
  value: T;
  onChange: (value: T) => void;
};

export function RadioGroup<T>(props: RadioGroupProps<T>): ReactElement {
  return (
    <div
      className={
        "nyx-radio-group" + (props.className ? ` ${props.className}` : "")
      }
      style={props.style}
    >
      {props.options.map((option) => (
        <Radio
          key={`${option.value}`}
          id={`${props.id}-${option.value}`}
          label={option.label}
          selected={option.value === props.value}
          onSelected={() => props.onChange(option.value)}
        />
      ))}
    </div>
  );
}
