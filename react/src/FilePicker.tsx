import * as React from "react";
import { ReactElement, useRef } from "react";
import { Button } from "./Button";

type FilePickerProps = {
  label: string;
  accept: string;
  onChange: (files: File[]) => void;
};

export function FilePicker(props: FilePickerProps): ReactElement {
  const ref = useRef<HTMLInputElement>(null);
  return (
    <>
      <Button label={props.label} onClick={() => ref.current!.click()} />
      <input
        type="file"
        style={{ display: "none" }}
        accept={props.accept}
        ref={ref}
        onChange={(e) => {
          props.onChange(Array.from(e.target.files || []));
        }}
      />
    </>
  );
}
