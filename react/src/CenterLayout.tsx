import * as React from "react";
import { ReactElement } from "react";

type CenterLayoutProps = {
  header: ReactElement;
  main: ReactElement;
};

export function CenterLayout(props: CenterLayoutProps): ReactElement {
  return (
    <div className="nyx-center-layout">
      {props.header}
      {props.main}
    </div>
  );
}
