import * as React from "react";
import { CSSProperties, ReactElement } from "react";

type CheckBoxProps = {
  className?: string;
  style?: CSSProperties;
  id?: string;
  label?: string;
  value: boolean;
  onChange: (value: boolean) => void;
};

export function CheckBox(props: CheckBoxProps): ReactElement {
  return (
    <div>
      <input
        className={
          "nyx-checkbox" + (props.className ? ` ${props.className}` : "")
        }
        style={props.style}
        id={props.id}
        type="checkbox"
        checked={props.value}
        onChange={(e) => props.onChange(e.target.checked)}
      />
      {props.label && <label htmlFor={props.id}>{props.label}</label>}
    </div>
  );
}
