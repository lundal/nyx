import * as React from "react";
import { faCaretDown, faCaretUp } from "@fortawesome/free-solid-svg-icons";
import { IconButton } from "./IconButton";
import { ReactElement, useState } from "react";
import { FocusTrap } from "./FocusTrap";

type App = {
  name: string;
  iconUrl: string;
  linkUrl: string;
};

type HeaderProps = {
  app: App;
  apps?: App[];
  actions?: ReactElement;
};

export function Header(props: HeaderProps): ReactElement {
  const [menuVisible, setMenuVisible] = useState(false);
  const [menuEnabled, setMenuEnabled] = useState(true);
  return (
    <header className="nyx-header">
      <a className="nyx-header--home" href={props.app.linkUrl}>
        <img src={props.app.iconUrl} alt="" />
        {props.app.name}
      </a>
      {props.apps && (
        <IconButton
          icon={menuVisible ? faCaretUp : faCaretDown}
          color="transparent"
          size="small"
          title={menuVisible ? "Hide apps" : "Show apps"}
          onClick={() => menuEnabled && setMenuVisible(!menuVisible)}
        />
      )}
      <div className="nyx-header--actions">{props.actions}</div>
      {props.apps && menuVisible && (
        <FocusTrap
          role="menu"
          className="nyx-header--apps"
          dismissOnBlur={true}
          onDismiss={() => {
            setMenuVisible(false);
            // Prevent reopen when clicking to close (hacky!)
            setMenuEnabled(false);
            setTimeout(() => setMenuEnabled(true), 300);
          }}
        >
          {props.apps.map((app) => (
            <a key={app.name} className="nyx-header--app" href={app.linkUrl}>
              <img src={app.iconUrl} alt="" />
              {app.name}
            </a>
          ))}
        </FocusTrap>
      )}
    </header>
  );
}
