import * as React from "react";
import { ReactElement, ReactNode } from "react";

type HeadingProps = {
  level: 1 | 2 | 3;
  size?: 1 | 2 | 3;
  children: ReactNode;
};

export function Heading(props: HeadingProps): ReactElement {
  const className = `nyx-h${props.size ?? props.level}`;

  switch (props.level) {
    case 1:
      return <h1 className={className}>{props.children}</h1>;
    case 2:
      return <h2 className={className}>{props.children}</h2>;
    case 3:
      return <h3 className={className}>{props.children}</h3>;
  }
}
