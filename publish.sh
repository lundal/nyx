#!/bin/bash
set -o errexit

version="$1"
modules="css react"

if [ -z "$version" ]
then
  echo "Usage: ./publish.sh <version>"
  exit 1
fi

if [ -n "$(git status --porcelain)" ]
then
  echo "Uncommitted changes:"
  git status --porcelain
  exit 1
fi

for module in $modules
do
  cd $module
  yarn version --no-git-tag-version --new-version $version
  cd ..
done

git commit --all --message "v$version"
git tag "v$version"

for module in $modules
do
  cd $module
  yarn install
  yarn clean
  yarn build
  yarn publish --access public --new-version $version
  cd ..
done

git push origin master

echo "Published $version"
